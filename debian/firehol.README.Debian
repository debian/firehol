FireHOL for Debian GNU/Linux
============================

FireHOL has to be enabled in /etc/default/firehol to
get started; to enable it set START_FIREHOL to anything
else than NO/no.

Variables set into /etc/default/firehol are exported into
by the initscript /etc/init.d/firehol, such that they are
carried out when firehol is launched from it while, otherwise,
they are generally ignored.

Handling FireHOL with ifupdowm-like machineries
===============================================

FireHOL is meant to be handled by a third-party software
like ifupdown (interfaces(5)) when the initscript parameter
START_FIREHOL is set to AUTO/auto. For now only the necessary
ifupdown material is provided. The FireHOL configuration file
/usr/share/doc/firehol/examples/ifupdown/firehol-ifupdown.conf
(distributed in the documentation package firehol-doc) exemplifies
the usage of this scheme with ifupdown and might be used as a template.

To be fully functional with the provided ifupdown material,
the actual FireHOL configuration /etc/firehol/firehol.conf file
must be marked/stamped as IFUPDOWN_COMPATIBLE_FIREHOL_CONFIG_FILE.
This is done by putting the entire ``word''
	IFUPDOWN_COMPATIBLE_FIREHOL_CONFIG_FILE
somewhere inside a comment. In the aforementioned example,
this marker was placed in the very first line of the file as follows:

==8><---------------------------------------------------------------
## firehol.conf IFUPDOWN_COMPATIBLE_FIREHOL_CONFIG_FILE example
---------------------------------------------------------------><8==

This maker is the guarantee by the system administrators that the
FireHOL configuration file handles the ifupdowm MODE stop properly.
When the maker is not matched, firehol is dictated to stop normally.
For further information and details, you may want to peruse interfaces(5)
and have a look to /etc/firehol/ifupdown-firehol.sh .

From a technical perspective, the scheme START_FIREHOL=AUTO neutralizes the
start and stop behaviours (commands) in the initscript /etc/init.d/firehol .
For this reason, the new behaviours force-start and force-stop have been
introduced to force the start and stop behaviours, respectively.


Log FireHOL messages to a separate log file with rsyslog [1]
============================================================

First set FIREHOL_LOG_PREFIX (firehol-variables(5)) to unique,
distinguishable prefix; let say, for illustration, to "FireHOL: ".

Then create /etc/rsyslog.d/firehol.conf with the following contents:

==8><---------------------------------------------------------------
:msg, contains, "FireHOL: " -/var/log/firehol/firehol.log
& stop
---------------------------------------------------------------><8==

what will make rsyslog to redirect FireHOL messages to the log file
`/var/log/firehol/firehol.log' within the directory `/var/log/firehol'
(created by hand).

A concomitant log rotation file may write:

==8><---------------------------------------------------------------
/var/log/firehol/firehol.log
{
	rotate 7
	daily
	missingok
	notifempty
	delaycompress
	compress
	postrotate
		invoke-rc.d rsyslog rotate > /dev/null
	endscript
}
---------------------------------------------------------------><8==

For more detailed information and discussion, visit the original blog page [1].

You can find these files in the `examples/rsyslog' folder in `/usr/share/doc/firehol-doc/'
as distributed by the firehol-doc Debian package.

[1] http://blog.shadypixel.com/log-iptables-messages-to-a-separate-file-with-rsyslog/


FireHOL, iptables-legacy(8), iptables-nft(8), and iptables
==========================================================

At the time of writing, FireHOL has no support for iptables-nft(8). That is,
FireHOL still uses iptables-legacy(8). This is not only very unfortunate but
also the source of numerous issues and confusions.

The transition from iptables[-legacy](8) to iptables-nft(8) occurs at Buster
(Debian 10), and in Buster iptables is managed as an alternative through
update-alternatives(981): the default is iptables-nft(8). However, FireHOL
explicitly uses iptables-legacy(8): the upgrading to Buster might involve
some manual interventions and material related to FireHOL might be forced
to use iptables-legacy(8) instead of iptables-nft(8).


 -- Jerome Benoit <calculus@rezozer.net>  Fri, 24 Jan 2024 17:19:23 +0000
