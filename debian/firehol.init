#! /bin/sh
### BEGIN INIT INFO
# Provides:          firehol
# Required-Start:    $network $remote_fs $syslog
# Required-Stop:     $network $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Description: Starts firehol firewall configuration
# short-description: firehol firewall configuration
### END INIT INFO

PATH=/bin:/usr/bin:/sbin:/usr/sbin
NAME=firehol
DESC="firewall"
SCRIPTNAME=/etc/init.d/$NAME

test -x /usr/sbin/firehol || exit 0

START_FIREHOL=NO
export START_FIREHOL
[ -r /etc/default/firehol ] && set -a && . /etc/default/firehol

# load the VERBOSE setting and other rcS variables
. /lib/init/vars.sh

# include lsb functions
. /lib/lsb/init-functions

case "$START_FIREHOL" in
  NO|no)
    START_FIREHOL=NO
  ;;
  AUTO|auto)
    START_FIREHOL=AUTO
  ;;
  *)
    START_FIREHOL=YES
  ;;
esac

do_metastart () {
	# return
	#  0 000 if firewall has been handled
	#  1 001 if firewall could not be activated
	#  2 010 if firewall is delegated to a third-party
	#  4 100 if FireHOL is disabled via /etc/default/firehol
	[ "$START_FIREHOL" = "NO"  ] && return 4
	[ "$START_FIREHOL" = "AUTO"  ] && return 2
	/usr/sbin/firehol start "$@" > /dev/null 2>&1 || return 1
}

do_start () {
	# return
	#  0 000 if firewall has been handled
	#  1 001 if firewall could not be activated
	#  4 100 if FireHOL is disabled via /etc/default/firehol
	[ "$START_FIREHOL" = "NO"  ] && return 4
	/usr/sbin/firehol start "$@" > /dev/null 2>&1 || return 1
}

do_metastop () {
	# return
	#  0 000 if firewall has been cleaned up properly
	#  1 001 if firewall could not be cleaned up properly
	#  2 010 if firewall is delegated to a third-party
	[ "$START_FIREHOL" = "AUTO"  ] && return 2
	/usr/sbin/firehol stop > /dev/null 2>&1 || return 1
}

do_stop () {
	# return
	#  0 000 if firewall has been cleaned up properly
	#  1 001 otherwise
	/usr/sbin/firehol stop > /dev/null 2>&1 || return 1
}

do_condrestart () {
	# return
	#  0 000 if firewall has been handled
	#  1 001 if firewall could not be activated
	#  4 100 if FireHOL is disabled via /etc/default/firehol
	[ "$START_FIREHOL" = "NO"  ] && return 4
	/usr/sbin/firehol condrestart "$@" > /dev/null 2>&1 || return 1
}

COMMAND="$1"
[ "$COMMAND" ] && shift

case "$COMMAND" in
	start)
		[ "$VERBOSE" != no ] && log_daemon_msg "Starting $DESC" "$NAME"
		do_metastart "$@"
		case "$?" in
			0) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
			1) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
			2) [ "$VERBOSE" != no ] && { log_progress_msg "delegated to a third-party" ; log_end_msg 0 ; } ;;
			4) [ "$VERBOSE" != no ] && { log_progress_msg "disabled, see /etc/default/firehol" ; log_end_msg 255 ; } ;;
		esac
	;;

	stop)
		[ "$VERBOSE" != no ] && log_daemon_msg "Stopping $DESC" "$NAME"
		do_metastop
		case "$?" in
			0) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
			1) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
			2) [ "$VERBOSE" != no ] && { log_progress_msg "delegated to a third-party" ; log_end_msg 0 ; } ;;
		esac
	;;

	condrestart)
		log_daemon_msg "Conditionally restarting $DESC" "$NAME"
		do_condrestart "$@"
		case "$?" in
			0) log_end_msg 0 ;;
			1) log_end_msg 1 ; exit 1 ;;
			4) log_progress_msg "disabled, see /etc/default/firehol" ; log_end_msg 255 ; ;;
		esac
	;;

	restart)
		log_daemon_msg "Restarting $DESC" "$NAME"
		do_metastart "$@"
		case "$?" in
			0) log_end_msg 0 ;;
			1) log_end_msg 1 ;;
			2) log_progress_msg "delegated to a third-party" ; log_end_msg 0 ; ;;
			4) log_progress_msg "disabled, see /etc/default/firehol" ; log_end_msg 255 ; ;;
		esac
	;;

	force-reload)
		log_daemon_msg "Restarting $DESC" "$NAME"
		do_start "$@"
		case "$?" in
			0) log_end_msg 0 ;;
			1) log_end_msg 1 ; exit 1 ;;
			4) log_progress_msg "disabled, see /etc/default/firehol" ; log_end_msg 255 ; ;;
		esac
	;;

	force-start)
		[ "$VERBOSE" != no ] && log_daemon_msg "Starting $DESC" "$NAME"
		do_start "$@"
		case "$?" in
			0) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
			1) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
			4) [ "$VERBOSE" != no ] && { log_progress_msg "disabled, see /etc/default/firehol" ; log_end_msg 255 ; } ;;
		esac
	;;

	force-stop)
		[ "$VERBOSE" != no ] && log_daemon_msg "Stopping $DESC" "$NAME"
		do_stop
		case "$?" in
			0) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
			1) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
		esac
	;;

	status)
		case "$START_FIREHOL" in
			NO)
				log_warning_msg "$DESC $NAME disabled via /etc/default/firehol"
				exit 0
				;;
			AUTO)
				log_success_msg "$DESC $NAME delegated via /etc/default/firehol"
				exit 4
				;;
			YES)
				log_success_msg "$DESC $NAME enabled via /etc/default/firehol"
				exit 4
				;;
			*)
				log_success_msg "$DESC $NAME confused by /etc/default/firehol"
				exit 4
				;;
		esac
	;;

	*)
	echo "Usage: $SCRIPTNAME {start|stop|condrestart|restart|force-reload|force-start|force-stop|status|helpme|wizard} [<args>]" >&2
	exit 3
	;;
esac

:
